from django.db import models

# # Create your models here.
class Season(models.Model):
    year = models.IntegerField(primary_key=True)
    
    def __str__(self):
        return str(self.year)
    class Meta:
        db_table = 'Season'
        
class Event(models.Model):
    season = models.ForeignKey("Season", on_delete=models.CASCADE)
    event_name = models.CharField(max_length=100)
    
    def __str__(self):
        # return f"{str(self.season)} {str(self.event_name)}"
        return str(self.event_name)

# class Event(models.Model):
#     round_number = models.PositiveIntegerField()
#     country = models.CharField(max_length=100)
#     location = models.CharField(max_length=100)
#     official_event_name = models.CharField(max_length=100)
#     event_name = models.CharField(max_length=100)
#     event_date = models.DateField()
#     event_format = models.CharField(max_length=100)
#     session = models.CharField(max_length=100)
#     session_date = models.DateTimeField()
#     session_date_utc = models.DateTimeField()
#     f1_api_support = models.BooleanField()

# class Session(models.Model):
#     event_id = models.ForeignKey("Event", on_delete=models.CASCADE)
#     name = models.CharField(max_length=100)
#     f1_api_support = models.BooleanField()
#     date = models.DateTimeField()
#     api_path = models.CharField(max_length=100)
#     session_info # dict
#     drivers # list
#     results # SessionResults (as a seperate table? Foreign key?)
#     total_laps = models.PositiveIntegerField()
#     weather_data # Dataframe
#     car_data # Dictionary from Telemetry
#     pos_data # Dictionary from Telemetry
#     session_status # Dataframe
#     track_status # Dataframe
#     race_control_messages # Dataframe
#     session_start_time = models.DurationField()
#     t0_date = models.DateTimeField()

# class Lap(models.Model):
#     session_id = models.ForeignKey("Session", on_delete=models.CASCADE)
#     time = models.DurationField()
#     lap_time = models.DurationField()
#     lap_number = models.PositiveIntegerField()
#     sector_1_time = models.DurationField()
#     sector_2_time = models.DurationField()
#     sector_3_time = models.DurationField()
#     sector_1_session_time = models.DurationField()
#     sector_2_session_time = models.DurationField()
#     sector_3_session_time = models.DurationField()
#     compound = models.CharField(max_length=100)
#     tyre_life = models.PositiveIntegerField()
#     stint = models.PositiveIntegerField()
#     lap_start_time = models.DurationField()
#     team = models.CharField(max_length=100)
#     driver = models.CharField(max_length=3)
#     driver_number = models.PositiveIntegerField()
#     track_status = models.PositiveIntegerField()

# class Results(models.Model):
#     session_id = models.ForeignKey("Session", on_delete=models.CASCADE)
#     driver_number = models.CharField(max_length=100)
#     broadcast_name = models.CharField(max_length=100)
#     full_name = models.CharField(max_length=100)
#     abbreviation = models.CharField(max_length=100)
#     driver_id = models.CharField(max_length=100)
#     team_name = models.CharField(max_length=100)
#     team_colour = models.CharField(max_length=100)
#     team_id = models.CharField(max_length=100)
#     first_name = models.CharField(max_length=100)
#     last_name = models.CharField(max_length=100)
#     headshot_url = models.CharField(max_length=100)
#     country_code = models.CharField(max_length=100)
#     position = models.FloatField(max_length=100)
#     classified_position = models.CharField(max_length=100)
#     grid_position = models.FloatField(max_length=100)
#     q1 = models.DurationField(max_length=100)
#     q2 = models.DurationField(max_length=100)
#     q3 = models.DurationField(max_length=100)
#     time = models.CharField(max_length=100)
#     status = models.CharField(max_length=100)
#     points = models.FloatField(max_length=100)

# class WeatherData(models.Model):
#     session_id = models.ForeignKey("Session", on_delete=models.CASCADE)
#     time = models.DurationField()
#     air_temp = models.FloatField()
#     humidity = models.FloatField()
#     pressure = models.FloatField()
#     rainfall = models.BooleanField()
#     track_temp = models.FloatField()
#     wind_direction = models.FloatField()
#     wind_speed = models.FloatField()

# class CarData(models.Model):
#     session_id = models.ForeignKey("Session", on_delete=models.CASCADE)
#     driver # Foreign key? Something else???
#     date = models.DateTimeField()
#     rpm = models.PositiveIntegerField()
#     speed = models.PositiveIntegerField()
#     gear = models.PositiveIntegerField()
#     throttle = models.PositiveIntegerField()
#     brake = models.BooleanField()
#     drs = models.PositiveIntegerField()
#     time = models.DurationField()
#     session_time = models.DurationField() # No difference between time and session_time - Redundant?

# class PositionData(models.Model):
#     session_id = models.ForeignKey("Session", on_delete=models.CASCADE)
#     driver # Foreign key? Something else???
#     date = models.DateTimeField()
#     status = models.CharField(max_length=100)
#     x_position = models.IntegerField()
#     y_position = models.IntegerField()
#     z_position = models.IntegerField()
#     time = models.DurationField()
#     session_time = models.DurationField() # No difference between time and session_time - Redundant?

# class SessionStatus(models.Model):
#     session_id = models.ForeignKey("Session", on_delete=models.CASCADE)
#     time = models.DurationField()
#     status = models.CharField(max_length=100)

# class TrackStatus(models.Model):
#     session_id = models.ForeignKey("Session", on_delete=models.CASCADE)
#     time = models.DateTimeField()
#     status = models.CharField(max_length=100)
#     message = models.CharField(max_length=100)

# class RaceControlMessages(models.Model):
#     session_id = models.ForeignKey("Session", on_delete=models.CASCADE)
#     time = models.DateTimeField()
#     category = models.CharField(max_length=100)
#     message = models.CharField(max_length=100)
#     status = models.CharField(max_length=100)
#     flag = models.CharField(max_length=100)
#     scope = models.CharField(max_length=100)
#     sector = models.PositiveIntegerField()
#     racing_number = models.CharField(max_length=100)

# # The following models may be useful for tracking Pirelli compound selections,
# # And the pace deltas, lifespan, and 'cliffs' between them.
# # EG: C2=Hard, C3=Medium, C4=Soft
# class Compound():
#     compound_name = models.CharField(max_length=100)

# class Tyre():
#     tyre_name = models.CharField(max_length=100)
#     tyre_colour = models.CharField(max_length=100)

# class CompoundSelection():
#     event_id = models.ForeignKey("Event", on_delete=models.CASCADE)
#     compound_id = models.ForeignKey("Compound", on_delete=models.CASCADE)
#     tyre_id = models.ForeignKey("Tyre", on_delete=models.CASCADE)