from django import forms
from .models import Event, Season

class EventForm(forms.Form):
    season = forms.ModelChoiceField(queryset=Season.objects.all(),
        widget = forms.Select(attrs={"hx-get": "load_events", "hx-target": "#id_event"}))
    event = forms.ModelChoiceField(queryset=Event.objects.none())
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        if "season" in self.data:
            season_id = int(self.data.get("season"))
            self.fields["event"].queryset = Event.objects.filter(season=season_id)