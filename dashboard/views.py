from django.shortcuts import render
import calculator.test_graphs as tg
from .models import Season, Event
from .forms import EventForm

# Create your views here.
def Dashboard(request):
    selected_season = 0
    selected_event = ""
    form = EventForm()
    if request.method=="POST":
        form = EventForm(request.POST)
        if form.is_valid():
            selected_season = int(form.data["season"])
            selected_event = str(form.cleaned_data["event"])
        else:
            print(form.errors)
    else:
        selected_season = 2021
        selected_event = "Abu Dhabi Grand Prix"
    
    race_name = " ".join([str(selected_season), selected_event, "Race Data"])
        
    gap_between_drivers, position_change, tyre_history, gap_to_leader = tg.get_race_data(selected_season, selected_event, "R")
    context = {
        'gap_between_drivers': gap_between_drivers,
        'position_change': position_change,
        'tyre_history': tyre_history,
        'gap_to_leader': gap_to_leader,
        'form': form,
        'race_name' : race_name
        }
    response = render(request, 'dashboard/dashboard.html', context=context)
    return response

def Load_Events(request):
    season_id = request.GET.get("season")
    events = Event.objects.filter(season=season_id)
    return render(request, "dashboard/event_options.html", {"events": events})