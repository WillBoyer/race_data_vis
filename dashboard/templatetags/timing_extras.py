from django import template
from math import floor
import pandas as pd

register = template.Library()


@register.filter
def delta_format(timedelta):
    time = "None"

    if pd.isnull(timedelta) == False:
        mls = floor(timedelta.microseconds/1000)
        sec = timedelta.seconds
        mins = 0

        if sec < 60:
            time = f"{sec:02d}.{mls:03d}"
        # Factor out how many minutes there are
        if sec >= 60:
            mins = floor(sec / 60)
            sec -= (mins * 60)
            time = f"{mins:02d}:{sec:02d}.{mls:03d}"
        # Factor out how many hours there are
        if mins >= 60:
            hrs = floor(mins / 60)
            mins -= (hrs * 60)
            time = f"{hrs}:{mins:02d}:{sec:02d}.{mls:03d}"
    return time