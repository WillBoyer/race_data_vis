# Brief Description
Very early stages of a Django-based web application to analyse and visualise motorsport race data.
Race data is gathered using the FastF1 Python package, analysed with Pandas and NumPy, and visualised with Plotly.

# Planned Features
- Estimating the Relative Time Lost During Pit-stops at Each Track
- Comparing the Effects of Tyre Degradation Between Teams
- Improved Tyre Strategy graph
