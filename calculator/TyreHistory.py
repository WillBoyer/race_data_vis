from matplotlib import pyplot as plt
import numpy as np
import fastf1 as ff1
from fastf1 import plotting
import pandas as pd

# plt.style.use('seaborn-v0_8')

session = ff1.get_session(2018, 'Hungarian Grand Prix', 'R')
session.load(telemetry=False, weather=False)
laps = session.laps
results = session.results

drivers = laps.Driver.unique() # Create list of all drivers

# Aggregate information from laps data (E.G. Count number of laps)
stint_df = pd.DataFrame(columns=['Driver', 
                                 'StintNo', 
                                 'StintLength', 
                                 'TyreCompound'])

for driver in drivers:
    driver_laps = session.laps.pick_driver(driver)
    
    for name, group in driver_laps.groupby('Stint', as_index=False):
        stint_no = group['Stint'].iloc[0]
        stint_length = len(group.index)
        tyre_compound = group['Compound'].iloc[0]
        position = results.loc[results['Abbreviation'] == driver, 'Position'].iloc[0]

        # Add values to dataframe
        stint_info = {'Driver': driver,
                      'StintNo': stint_no,
                      'StintLength':stint_length, 
                      'TyreCompound': tyre_compound,
                      'Position': position}
        
        stint_df = pd.concat([stint_df, pd.DataFrame([stint_info])], ignore_index=True)

width = 0.5

fig, ax = plt.subplots()

print(stint_df)
stint_df = stint_df.sort_values(by = ['Position', 'StintNo'])

print(stint_df)

for name, group in stint_df.groupby('Driver', as_index=False):
    left = 0
    for index, row in group.iterrows():
        bar = ax.barh(y=row['Driver'], 
                      width=row['StintLength'], 
                      left=left,
                      edgecolor="#000000",
                      color=plotting.COMPOUND_COLORS[row['TyreCompound']],
                      label=row['TyreCompound'])
        left += row['StintLength']


ax.set_ylabel('Laps Completed')
plt.suptitle(f"Tyre History for Each Driver\n"
             f"{session.event['EventName']} {session.event.year}")

handles, labels = plt.gca().get_legend_handles_labels()
by_label = dict(zip(labels, handles))

ax.legend(by_label.values(), by_label.keys(), bbox_to_anchor=(1.0, 1.02))
plt.tight_layout()

plt.show()

# How to sort bars by finishing position?
# Why are extra stints being added?