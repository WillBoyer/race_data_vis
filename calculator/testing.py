from matplotlib import pyplot as plt
import fastf1 as ff1
from fastf1 import plotting
import pandas as pd

plt.style.use('seaborn-v0_8')

# Bar chart showing difference between lap times of two cars
# Additionally, plot showing gap in seconds between two cars

session = ff1.get_session(2021, 'Abu Dhabi Grand Prix', 'R')
session.load(telemetry=False, weather=False)