from matplotlib import pyplot as plt
import fastf1
from fastf1 import plotting
import pandas as pd
import numpy as np

plt.style.use('seaborn-v0_8')

session = fastf1.get_session(2023, 'Miami Grand Prix', 'R')
session.load(telemetry=False, weather=False)

race_laps = session.laps

drivers = race_laps.Driver.unique() # Create list of all drivers

fig,ax = plt.subplots()

gaps = []

laps_array = []
race_laps['GapToLeader'] = np.nan
for name, group in race_laps.groupby('LapNumber', as_index=False):
    leaderTime = None
    for index, row in group.iterrows():
        if row['Position'] == 1.0:
            leaderTime = row['Time']
        else:
            row['GapToLeader'] = (row.Time - leaderTime) / pd.Timedelta(seconds=1)
        laps_array.append(row)

race_laps['GapToLeader'] = laps_array
print(race_laps)

# Generating a plot for each driver
for driver in drivers:
    # Picking out laps specific to the driver
    driver_laps = race_laps.pick_driver(driver)
    
    # Using FastF1's pre-defined driver colours
    colour = fastf1.plotting.driver_color(driver)
    ax.plot(driver_laps['LapNumber'], driver_laps['GapToLeader'], label=driver, color=colour)


ax.set_xlabel('Lap Number')
ax.set_ylabel('Gap (s)')

plt.suptitle(f"Gaps to Leader in Seconds\n"
             f"{session.event['EventName']} {session.event.year}")

ax.legend(bbox_to_anchor=(1.0, 1.02))
plt.tight_layout()

plt.show()