from matplotlib import pyplot as plt
import fastf1 as ff1
from fastf1 import plotting
import pandas as pd

plt.style.use('seaborn-v0_8')

# Bar chart showing difference between lap times of two cars
# Additionally, plot showing gap in seconds between two cars

session = ff1.get_session(2021, 'Abu Dhabi Grand Prix', 'R')
session.load(telemetry=False, weather=False)

driver_a = "VER"
driver_b = "HAM"

relevant_cols = [
    'Time',
    'LapTime',
    'LapNumber',
    'Sector1Time',
    'Sector2Time',
    'Sector3Time',
    'Sector1SessionTime',
    'Sector2SessionTime',
    'Sector3SessionTime'
    ]

driver_a_laps = session.laps.pick_driver(driver_a).filter(relevant_cols).sort_values(by='LapNumber').reset_index()
driver_b_laps = session.laps.pick_driver(driver_b).filter(relevant_cols).sort_values(by='LapNumber').reset_index()
versus_df = pd.DataFrame(columns=[
        'LapNumber',
        'Interval',
        'LapTimeDiff',
        'Sector1Diff',
        'Sector2Diff',
        'Sector3Diff',
        'Sector1Int',
        'Sector2Int',
        'Sector3Int'
        ])

# 
versus_df['LapNumber'] = driver_a_laps.LapNumber
versus_df['Interval'] = (driver_b_laps.Time - driver_a_laps.Time) / pd.Timedelta(seconds=1)
versus_df['LapTimeDiff'] = (driver_b_laps.LapTime - driver_a_laps.LapTime) / pd.Timedelta(seconds=1)
versus_df['Sector1Diff'] = (driver_b_laps.Sector1Time - driver_a_laps.Sector1Time) / pd.Timedelta(seconds=1)
versus_df['Sector2Diff'] = (driver_b_laps.Sector2Time - driver_a_laps.Sector2Time) / pd.Timedelta(seconds=1)
versus_df['Sector3Diff'] = (driver_b_laps.Sector3Time - driver_a_laps.Sector3Time) / pd.Timedelta(seconds=1)
versus_df['Sector1Int'] = (driver_b_laps.Sector1SessionTime - driver_a_laps.Sector1SessionTime) / pd.Timedelta(seconds=1)
versus_df['Sector2Int'] = (driver_b_laps.Sector2SessionTime - driver_a_laps.Sector2SessionTime) / pd.Timedelta(seconds=1)
versus_df['Sector3Int'] = (driver_b_laps.Sector3SessionTime - driver_a_laps.Sector3SessionTime) / pd.Timedelta(seconds=1)

fig, ax = plt.subplots()

ax.plot(versus_df['LapNumber'], versus_df['LapTimeDiff'], color='black', label='Lap Time Gap')
ax.plot(versus_df['LapNumber'], versus_df['Interval'], linestyle='dashed', label='Interval Time Gap')
ax.fill_between(versus_df['LapNumber'], versus_df['LapTimeDiff'], where=(versus_df['LapTimeDiff'] > 0), color='#008000', interpolate=True, alpha=0.75, label=f"{driver_a} Faster")
ax.fill_between(versus_df['LapNumber'], versus_df['LapTimeDiff'], where=(versus_df['LapTimeDiff'] < 0), color='#ff0000', interpolate=True, alpha=0.75, label=f"{driver_a} Slower")

plt.legend()

ax.set_xlabel('Lap Number')
ax.set_ylabel('Time Gap (seconds)')

plt.suptitle(f"Lap Time Comparison and Interval Between {driver_a} and {driver_b}\n"
             f"{session.event['EventName']} {session.event.year}\n")

plt.tight_layout()

plt.show()

# Any way to add tables on mouse-over, similar to ALMS viewer?