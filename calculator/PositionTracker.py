from matplotlib import pyplot as plt
import fastf1
from fastf1 import plotting

plt.style.use('seaborn-v0_8')

session = fastf1.get_session(2023, 'Dutch Grand Prix', 'R')
session.load(telemetry=False, weather=False)

drivers = session.laps.Driver.unique() # Create list of all drivers

fig, ax = plt.subplots()

# Generating a plot for each driver
for driver in drivers:
    # Picking out laps specific to the driver
    driver_laps = session.laps.pick_driver(driver)
    
    # Using FastF1's pre-defined driver colours
    colour = fastf1.plotting.driver_color(driver)
    ax.plot(driver_laps['LapNumber'], driver_laps['Position'], label=driver, color=colour, marker='o')
    
ax.set_xlabel('Lap Number')
ax.set_ylabel('Position')
ax.set_yticks([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20])

plt.suptitle(f"Driver Positions Each Lap\n"
             f"{session.event['EventName']} {session.event.year}")

plt.gca().invert_yaxis()
ax.legend(bbox_to_anchor=(1.0, 1.02))
plt.tight_layout()

plt.show()

# Any way to identify and denote a pit stop lap?
    # Markers?
    # Annotations?
    # Apply markers and/or annotations conditionally?