import plotly.express as px
import plotly.graph_objects as go
import fastf1 as ff1
from fastf1 import plotting
import pandas as pd
from plotly.offline import plot
import numpy as np

def get_race_data(year, event, session):
    ff1.Cache.enable_cache('./calculator/cache')
    session = ff1.get_session(year, event, session)
    session.load(telemetry=False, weather=False)
    race_laps = session.laps

    gap_between_drivers = generate_gap_plot(race_laps)
    position_change = generate_position_tracker(race_laps)
    tyre_history = generate_tyre_history(session)
    gap_to_leader = generate_leader_gap(race_laps)
    return gap_between_drivers, position_change, tyre_history, gap_to_leader

# Bar chart showing difference between lap times of two cars
# Additionally, plot showing gap in seconds between two cars
def generate_gap_plot(race_laps):

    driver_a = "VER"
    driver_b = "HAM"

    relevant_cols = [
        'Time',
        'LapTime',
        'LapNumber',
        'Sector1Time',
        'Sector2Time',
        'Sector3Time',
        'Sector1SessionTime',
        'Sector2SessionTime',
        'Sector3SessionTime'
        ]

    driver_a_laps = race_laps.pick_driver(driver_a).filter(relevant_cols).sort_values(by='LapNumber').reset_index()
    driver_b_laps = race_laps.pick_driver(driver_b).filter(relevant_cols).sort_values(by='LapNumber').reset_index()
    versus_df = pd.DataFrame()

    versus_df['LapNumber'] = driver_a_laps.LapNumber
    versus_df['Interval'] = (driver_b_laps.Time - driver_a_laps.Time) / pd.Timedelta(seconds=1)
    versus_df['LapTimeDiff'] = (driver_b_laps.LapTime - driver_a_laps.LapTime) / pd.Timedelta(seconds=1)
    versus_df['Sector1Diff'] = (driver_b_laps.Sector1Time - driver_a_laps.Sector1Time) / pd.Timedelta(seconds=1)
    versus_df['Sector2Diff'] = (driver_b_laps.Sector2Time - driver_a_laps.Sector2Time) / pd.Timedelta(seconds=1)
    versus_df['Sector3Diff'] = (driver_b_laps.Sector3Time - driver_a_laps.Sector3Time) / pd.Timedelta(seconds=1)
    versus_df['Sector1Int'] = (driver_b_laps.Sector1SessionTime - driver_a_laps.Sector1SessionTime) / pd.Timedelta(seconds=1)
    versus_df['Sector2Int'] = (driver_b_laps.Sector2SessionTime - driver_a_laps.Sector2SessionTime) / pd.Timedelta(seconds=1)
    versus_df['Sector3Int'] = (driver_b_laps.Sector3SessionTime - driver_a_laps.Sector3SessionTime) / pd.Timedelta(seconds=1)
    
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=versus_df['LapNumber'], y=versus_df['LapTimeDiff'],
                             mode='lines', name='Lap Delta',
                             hovertemplate=driver_a + " %{y} seconds vs. " + driver_b))
    fig.add_trace(go.Scatter(x=versus_df['LapNumber'], y=versus_df['Interval'],
                             mode='lines', name='Total Gap',
                             hovertemplate=driver_a + " %{y} seconds to " + driver_b))
    fig.update_xaxes(title_text='Lap')
    fig.update_yaxes(title_text="Time (seconds)")
    fig.update_layout(width=1600, height=800, hovermode="x unified",
                      title={'text': "Lap Delta and Total Time Gap Between {} and {}".format(driver_a, driver_b)})
    
    plt_div = plot(fig, output_type='div', include_plotlyjs=False, show_link=False, link_text="")

    return plt_div

def generate_position_tracker(race_laps):
    fig = px.line(race_laps, x="LapNumber", y="Position", color="Driver",
                  labels=dict(LapNumber="Lap", Position="Position"),
                  title="Position for Each Driver at the End of Each Lap",
                  width=1600, height=800)
    fig['layout']['yaxis']['autorange'] = 'reversed'
    plt_div = plot(fig, output_type='div', include_plotlyjs=False, show_link=False, link_text="")
    return plt_div

def generate_tyre_history(session):
    race_laps = session.laps
    drivers = session.drivers
    drivers = [session.get_driver(driver)["Abbreviation"] for driver in drivers]
    print(drivers)

    no_of_laps = len(race_laps.pick_driver(drivers[0]))

    stint_df = pd.DataFrame(columns=['Driver',
                                     'StintNo',
                                     'OutLap',
                                     'InLap',
                                     'StintLength',
                                     'Delta',
                                     'TyreCompound'])
    
    
    for driver in drivers:
        driver_laps = race_laps.pick_driver(driver)

        for name, group in driver_laps.groupby('Stint', as_index=False):
            stint_no = int(group['Stint'].iloc[0])
            stint_length = len(group.index)
            tyre_compound = group['Compound'].iloc[0]
            out_lap = group['LapNumber'].min()
            in_lap = group['LapNumber'].max()
            delta = in_lap - out_lap

            stint_info = {'Driver': driver,
                          'StintNo': stint_no,
                          'OutLap': out_lap,
                          'InLap': in_lap,
                          'StintLength': stint_length,
                          'Delta': delta,
                          'TyreCompound': tyre_compound}
            
            stint_df = pd.concat([stint_df, pd.DataFrame([stint_info])], ignore_index=True)


    stint_df = stint_df.sort_values(by=['Driver', 'StintNo'])
    
    print(stint_df)
    
    fig = px.timeline(stint_df,
                      x_start="OutLap", 
                      x_end="InLap", 
                      y="Driver", 
                      color="TyreCompound", color_discrete_map=ff1.plotting.COMPOUND_COLORS,
                      category_orders={"TyreCompound": ["SOFT", "MEDIUM", "HARD", "INTERMEDIATE", "WET"]})
    fig.update_yaxes(autorange="reversed")
    fig.layout.xaxis.type = 'linear'

    for d in fig.data:
        filt = stint_df['TyreCompound'] == d.name
        d.x = stint_df[filt]['Delta'].tolist()

    fig.update_layout(autosize=False,
                      width=1600, 
                      height=800, 
                      xaxis_title="Lap", 
                      xaxis={'range':[1,no_of_laps]})
    plt_div = plot(fig, output_type='div', include_plotlyjs=False, show_link=False, link_text="")
    return plt_div

def generate_leader_gap(race_laps):
    laps_array = []
    
    relevant_cols = [
        'Time',
        'LapNumber',
        'Driver',
        'Position'
    ]

    # Remove irrelevant columns from dataframe
    race_laps = race_laps.filter(relevant_cols).sort_values(by=['LapNumber', 'Position'])
    
    race_laps['GapToLeader'] = 0
    for name, group in race_laps.groupby('LapNumber', as_index=False):
        leaderTime = None
        for index, row in group.iterrows():
            if row['Position'] == 1.0:
                leaderTime = row['Time']
            else:
                row['GapToLeader'] = (row.Time - leaderTime) / pd.Timedelta(seconds=1)
            laps_array.append(row)
    new_laps = pd.DataFrame(laps_array, index=None)
    
    fig = px.line(new_laps, x="LapNumber", y="GapToLeader", color="Driver",
                  labels=dict(LapNumber="Lap", GapToLeader="Gap To Leader (seconds)"),
                  title="Gap to Leader for Each Car",
                  width=1600, height=800)
    
    plt_div = plot(fig, output_type='div', include_plotlyjs=False, show_link=False, link_text="")
    return plt_div