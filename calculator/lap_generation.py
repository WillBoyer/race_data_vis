import fastf1 as ff1
import pandas as pd
import numpy as np


def add_position(laps_data):
    laps_data['Position'] = laps_data.groupby('LapNumber')['Time'].rank()
    return laps_data

def add_gap_leader(laps_data):
    laps_array = []
    laps_data['GapToLeader'] = np.nan
    for name, group in laps_data.groupby('LapNumber', as_index=False):
        leaderTime = None
        for index, row in group.iterrows():
            if row['Position'] == 1.0:
                leaderTime = row['Time']
            else:
                row['GapToLeader'] = row.Time - leaderTime
            laps_array.append(row)
    new_laps = pd.DataFrame(laps_array, index=None)
    return new_laps

def add_interval(laps_data):
    laps_array = []
    laps_data['Interval'] = np.nan
    # Iterate over laps and/or drivers (except DNFs)
    for name, group in laps_data.groupby('LapNumber', as_index=False):
        group = group.sort_values(by=['Position'])
        driverAhead = None
        for index, row in group.iterrows():
            driverPosition = row['Position']
            if driverPosition > 1:
                # Subtract driver ahead's time from driver behind
                # Add delta as an 'Interval' column to the dataframe
                row['Interval'] = row.Time - driverAhead.Time
            laps_array.append(row)
            # As we progress one position at a time, the currently-selected driver is
            # directly ahead of the next selected driver
            driverAhead = row
    new_laps = pd.DataFrame(laps_array, index=None)
    return new_laps

"""Takes drivers who have not finished (or in some cases, not started) the race
 and re-adds them to the remaining laps. This equalises the number of rows for 
 each lap of the race, which is necessary to confine each lap's data to its own
 single page"""
def add_dnfs(laps):
    laps_array = []
    # Get list of all competing drivers
    drivers = laps.drop_duplicates('Driver')
    drivers = drivers[['Driver', 'DriverNumber', 'Team']]
    # For DNF: check which drivers are missing from race laps
    for name, group in laps.groupby('LapNumber', as_index=False):
        for index, driver in drivers.iterrows():
            if driver['Driver'] not in group.Driver.values:
                # Create new lap data for driver
                print("Creating new lap data")
                added_lap = {
                    'Time': np.NaN,
                    'LapTime': np.NaN,
                    'LapNumber': group['LapNumber'].values[0],
                    'Sector1Time': np.NaN,
                    'Sector2Time': np.NaN,
                    'Sector3Time': np.NaN,
                    'Sector1SessionTime': np.NaN,
                    'Sector2SessionTime': np.NaN,
                    'Sector3SessionTime': np.NaN,
                    'Compound': np.NaN,
                    'TyreLife': np.NaN,
                    'Stint': np.NaN,
                    'LapStartTime': np.NaN,
                    'Team': driver['Team'],
                    'Driver': driver['Driver'],
                    'DriverNumber': driver['DriverNumber'], 
                    'TrackStatus': np.NaN
                }
                print(added_lap)
                laps_array.append(added_lap)

    new_laps = pd.DataFrame(laps_array, index=None)
    laps = pd.concat([laps, new_laps])
    # Remove Lap 0
    # laps = laps.drop(laps[laps.LapNumber == 0].index)
    laps = laps[laps.LapNumber > 0]
    return laps

def get_laps(year, event, session):
    ff1.Cache.enable_cache("~/Documents/Programming_Practice/CalcF1/cache")
    race = ff1.get_session(year, event, session)
    race.load(telemetry=False, weather=False)
    laps = race.laps

    relevant_cols = [
        'Time',
        'LapTime',
        'LapNumber',
        'Sector1Time',
        'Sector2Time',
        'Sector3Time',
        'Sector1SessionTime',
        'Sector2SessionTime',
        'Sector3SessionTime',
        'Compound',
        'TyreLife',
        'Stint',
        'LapStartTime',
        'Team',
        'Driver',
        'DriverNumber',
        'TrackStatus'
    ]

    # Remove irrelevant columns from dataframe
    laps = laps.filter(relevant_cols)

    # Number of pitstops more useful than current stint count
    laps['PitStops'] = laps['Stint'] - 1

    laps = add_position(laps)
    laps = add_gap_leader(laps)
    laps = add_interval(laps)
    laps = add_dnfs(laps)
    laps = laps.sort_values(['LapNumber', 'Position'], ascending = True)

    return laps