colours_2019 = 

colours_2020 = {'Mercedes': '#00d2be',
                'Ferrari': '#dc0000',
                'Red Bull': '#1e41ff',
                'McLaren': '#ff8700',
                'Renault': '#fff500',
                'Racing Point': '#f596c8',
                'Alfa Romeo': '#9b0000',
                'Toro Rosso': '#469bff',
                'Haas F1 Team': '#7e7e7e',
                'Williams': '#469bff',
                'Alpha Tauri': '#ffffff'
                }

colours_2021 = {'Mercedes': '#00d2be', 
                'Ferrari': '#dc0000',
                'Red Bull': '#0600ef', 
                'McLaren': '#ff8700',
                'Alpine': '#0090ff', 
                'Aston Martin': '#006f62',
                'Alpine F1 Team': '#0090ff',
                'Alfa Romeo': '#900000',
                'AlphaTauri': '#2b4562',
                'Haas F1 Team': '#ffffff', 
                'Williams': '#005aff'
                }

colours_2022 = 

colours_2023 = 

colours_2024 = {'mercedes': '#00d2be', 
                'ferrari': '#dc0000',
                'red bull': '#fcd700', 
                'mclaren': '#ff8700',
                'alpine': '#fe86bc', 
                'aston martin': '#006f62',
                'sauber': '#00e701', 
                'visa rb': '#1634cb',
                'haas': '#ffffff', 
                'williams': '#00a0dd'
                }